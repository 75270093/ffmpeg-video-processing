### 介绍
ffmpeg服务器搭建立：
https://www.cnblogs.com/ramlife/p/16575589.html


通过ffmpeg工具对视频的各种操作，包含获取视频长宽、播放时长、m3u8类型视频（非直播）链接下载、视频合并、视频截图、压缩视频等等，具体参看类：
 
最新视频去重步骤：
1、绿幕：新增一个绿幕做为背景，透明度最低。
2、改版去重：现有的字幕，比例，变速等。
3、扫光(特效)：页面有一道光的特效，从不多角度走到对角，或从上到下，从下到上。特效时长根据视频长度截取。
4、光的调节(正负5锐化25(再次改版) 。
5、贴纸，现有的贴图长宽改为最小，看不到为止。
7、绿幕(再次去重)：新增一个绿幕前景，透明度最低。
8、下钩子(黄金3秒)：在前几秒或结束几秒加上文字，提示在哪里能得到1元团+评论区。
9、封面：要求上传或视频截取一个封面，加大文字标题。


utils.com.pinjiule.ffmpeg.FFfmpegUtils[FfmpegUtils.java](src%2Fmain%2Fjava%2Fcom%2Fpinjiule%2Fffmpeg%2Futils%2FFfmpegUtils.java)

```java
public class VideoInfoUtil {

    private static final Logger log = LoggerFactory.getLogger(VideoInfoUtil.class);
    /**
     * 通过视频文件获取视频信息
     *
     * @param videoPath
     * @return
     */
    public static MultimediaInfo getVideoInfoByFile(String videoPath) {
        try {
            File file = new File(videoPath);
            Encoder encoder = new Encoder();
            MultimediaInfo m = encoder.getInfoByFile(file);
            if (null != m) {
                m.setVideoSize(file.length());
            }
            return m;
        } catch (Exception e) {
            log.error("获取播放播放时长异常 videoPath=" + videoPath, e);
        }
        return null;
    }

    /**
     * 通过视频地址获取视频信息
     *
     * @param videoUrl
     * @return
     */
    public static MultimediaInfo getVideoInfoByUrl(String videoUrl, String ua, int timeout, boolean ifProxy) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            MultimediaInfo m = encoder.getInfoByUrl(videoUrl, ua,timeout, ifProxy);
            long end = System.currentTimeMillis();
            log.info("获取视频宽高时长,duration={}; 耗时={}", m.getDuration(), (end - start));
            return m;
        } catch (Exception e) {
            log.error("获取视频信息异常 videoUrl=" + videoUrl, e);
        }
        return null;
    }

    /**
     * 下载m3u8视频
     *
     * @param url    m3u8播放地址
     * @param output 视频输出路径
     * @return
     */
    public static boolean downloadAndMergeM3U8Video(String url, String output) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            boolean b = encoder.mergeM3U8Video(url, output);
            long end = System.currentTimeMillis();
            log.info("url={} output={} m3u8视频耗时={}", url, output, (end - start));
            return b;
        } catch (Exception e) {
            log.error("合并视频异常 url={} output={}", url, output, e);
        }
        return false;
    }

    /**
     * 合并视频
     *
     * @param output 视频的输出位置
     * @param input  分段视频
     * @return
     */
    public static boolean mergeVideo(String output, List<String> input) {
        try {
            if (null == output || null == input) {
                return false;
            }
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            boolean b = encoder.mergeVideo(output, input.toArray(new String[input.size()]));
            long end = System.currentTimeMillis();
            log.info("input={} output={} 合并视频耗时={}", input, output, (end - start));
            return b;
        } catch (Exception e) {
            log.error("合并视频异常 input=" + input + " output" + output, e);
        }
        return false;
    }

    /**
     * 截封面图
     *
     * @param input     视频文件或地址
     * @param time      截图的固定时间点
     * @param imgOutPut 图片的输出路径
     * @return 是否成功
     */
    public static boolean videoScreenshot(String input, String time, String imgOutPut) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            String imgPath = null;
            if (null == imgOutPut) {
                File temp = new File(System.getProperty("user.dir"), "work");
                if (!temp.exists()) {
                    temp.mkdirs();
                }
                imgPath = temp.getAbsolutePath() + File.separator + UUID.randomUUID().toString() + ".png";
            } else {
                imgPath = imgOutPut;
            }
            boolean b = encoder.videoScreenshot(input, time, imgPath);
            long end = System.currentTimeMillis();
            log.info("input={} imgPath={} 截图耗时={}", input, imgOutPut, (end - start));
            return b;
        } catch (Exception e) {
            log.error("视频截图异常 time=" + time + " output" + input, e);
        }
        return false;
    }

    /**
     * 压缩视频
     *
     * @param output
     * @param input
     * @return
     */
    public static boolean compressVideo(String output, String input) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            boolean b = encoder.compressVideo(output, input);
            long end = System.currentTimeMillis();
            log.info("input=" + input + " output=" + output + "压缩视频耗时=" + (end - start));
            return b;
        } catch (Exception e) {
            log.error("压缩视频异常 input=" + input + " output" + output, e);
        }
        return false;
    }


}
```

