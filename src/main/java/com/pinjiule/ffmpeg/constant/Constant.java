package com.pinjiule.ffmpeg.constant;

/**
 * @author LiYaQ
 * @date Created in 16:18 2020/10/22
 */
public interface Constant {
    String PROXY_IP = "";
    int PROXY_PORT = 80;
}
