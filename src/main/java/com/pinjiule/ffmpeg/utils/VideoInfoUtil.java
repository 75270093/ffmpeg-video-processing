package com.pinjiule.ffmpeg.utils;

import com.pinjiule.ffmpeg.ffmpeg.Encoder;
import com.pinjiule.ffmpeg.ffmpeg.MultimediaInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * @author: LiYaQ
 * @description:
 * @date: Created in 2019/3/10 11:45
 * @modified By:
 */
public class VideoInfoUtil {

    private static final Logger log = LoggerFactory.getLogger(VideoInfoUtil.class);

    public static void main(String[] args) {
        //System.out.println(compressVideo("E:\\data\\2.mp4","E:\\data\\1.mp4"));
        //System.out.println(videoScreenshot("http://c7c6ea5c95c6a.cdn.sohucs.com/shortvideo/20d8bbda-0dff-4475-8217-e60d55e04650.mp4", "2.00"));
        System.out.println("开始=====");
        long start = System.currentTimeMillis();
        MultimediaInfo info = getVideoInfoByUrl("http://120.78.86.132:9090/pjl/douyin/111.mp4", null, 3,false);
        long end = System.currentTimeMillis();
        System.out.println("耗时=" + (end - start));

        System.out.println(info.getDuration());
        System.out.println(info.getVideo().getSize().getWidth());
        System.out.println(info.getVideo().getSize().getHeight());



    }




    /**
     * 通过视频文件获取视频信息
     *
     * @param videoPath
     * @return
     */
    public static MultimediaInfo getVideoInfoByFile(String videoPath) {
        try {
            File file = new File(videoPath);
            Encoder encoder = new Encoder();
            MultimediaInfo m = encoder.getInfoByFile(file);
            if (null != m) {
                m.setVideoSize(file.length());
            }
            return m;
        } catch (Exception e) {
            log.error("获取播放播放时长异常 videoPath=" + videoPath, e);
        }
        return null;
    }

    /**
     * 通过视频地址获取视频信息
     *
     * @param videoUrl
     * @return
     */
    public static MultimediaInfo getVideoInfoByUrl(String videoUrl, String ua, int timeout, boolean ifProxy) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            MultimediaInfo m = encoder.getInfoByUrl(videoUrl, ua,timeout, ifProxy);
            long end = System.currentTimeMillis();
            log.info("获取视频宽高时长,duration={}; 耗时={}", m.getDuration(), (end - start));
            return m;
        } catch (Exception e) {
            log.error("获取视频信息异常 videoUrl=" + videoUrl, e);
        }
        return null;
    }

    /**
     * 下载m3u8视频
     *
     * @param url    m3u8播放地址
     * @param output 视频输出路径
     * @return
     */
    public static boolean downloadAndMergeM3U8Video(String url, String output) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            boolean b = encoder.mergeM3U8Video(url, output);
            long end = System.currentTimeMillis();
            log.info("url={} output={} m3u8视频耗时={}", url, output, (end - start));
            return b;
        } catch (Exception e) {
            log.error("合并视频异常 url={} output={}", url, output, e);
        }
        return false;
    }

    /**
     * 合并视频
     *
     * @param output 视频的输出位置
     * @param input  分段视频
     * @return
     */
    public static boolean mergeVideo(String output, List<String> input) {
        try {
            if (null == output || null == input) {
                return false;
            }
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            boolean b = encoder.mergeVideo(output, input.toArray(new String[input.size()]));
            long end = System.currentTimeMillis();
            log.info("input={} output={} 合并视频耗时={}", input, output, (end - start));
            return b;
        } catch (Exception e) {
            log.error("合并视频异常 input=" + input + " output" + output, e);
        }
        return false;
    }

    /**
     * 截封面图
     *
     * @param input     视频文件或地址
     * @param time      截图的固定时间点
     * @param imgOutPut 图片的输出路径
     * @return 是否成功
     */
    public static boolean videoScreenshot(String input, String time, String imgOutPut) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            String imgPath = null;
            if (null == imgOutPut) {
                File temp = new File(System.getProperty("user.dir"), "work");
                if (!temp.exists()) {
                    temp.mkdirs();
                }
                imgPath = temp.getAbsolutePath() + File.separator + UUID.randomUUID().toString() + ".png";
            } else {
                imgPath = imgOutPut;
            }
            boolean b = encoder.videoScreenshot(input, time, imgPath);
            long end = System.currentTimeMillis();
            log.info("input={} imgPath={} 截图耗时={}", input, imgOutPut, (end - start));
            return b;
        } catch (Exception e) {
            log.error("视频截图异常 time=" + time + " output" + input, e);
        }
        return false;
    }

    /**
     * 压缩视频
     *
     * @param output
     * @param input
     * @return
     */
    public static boolean compressVideo(String output, String input) {
        try {
            long start = System.currentTimeMillis();
            Encoder encoder = new Encoder();
            boolean b = encoder.compressVideo(output, input);
            long end = System.currentTimeMillis();
            log.info("input=" + input + " output=" + output + "压缩视频耗时=" + (end - start));
            return b;
        } catch (Exception e) {
            log.error("压缩视频异常 input=" + input + " output" + output, e);
        }
        return false;
    }


}
